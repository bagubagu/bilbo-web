'use strict';
angular.module('app', [
    'ui.router',
    'ngMaterial',
    'ngMdIcons'

])
    .config(appConfig)
    .controller('appController', appController)

function appController ($scope, $mdSidenav, $mdDialog){
    $scope.openLeftMenu = function() {
        $mdSidenav('left').toggle();
    };
}

function appConfig ($stateProvider, $urlRouterProvider, $mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('red', {
            'default': '400', // by default use shade 400 from the pink palette for primary intentions

        })
        .accentPalette('grey', {
            'default': '700', // by default use shade 400 from the pink palette for primary intentions
            'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
            'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
            'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
        })
    $urlRouterProvider.otherwise('/home');
    $stateProvider
    .state('home', {
        url: '/home',
        views: {
            "viewMain": { templateUrl: 'components/home/home.html' },
        }
    })
    $stateProvider
        .state('findApartment', {
            url: '/findApartment',
            views: {
                "viewMain": { templateUrl: 'components/findApartment/findApartment.html' },
            }
        })
    $stateProvider
        .state('chooseRoom', {
            url: '/chooseRoom',
            views: {
                "viewMain": { templateUrl: 'components/chooseRoom/chooseRoom.html' },
            }
        })
    $stateProvider
        .state('host', {
            url: '/host-basics',
            views: {
                "viewMain": { templateUrl: 'components/host-basics/host-basics.html' },
            }
        })
    $stateProvider
        .state('host-location', {
            url: '/host-location',
            views: {
                "viewMain": { templateUrl: 'components/host-location/host-location.html' },
            }
        })
    $stateProvider
        .state('host-amenities', {
            url: '/host-amenities',
            views: {
                "viewMain": { templateUrl: 'components/host-amenities/host-amenities.html' },
            }
        })
    $stateProvider
        .state('host-photos', {
            url: '/host-photos',
            views: {
                "viewMain": { templateUrl: 'components/host-photos/host-photos.html' },
            }
        })
    $stateProvider
        .state('host-pricing', {
            url: '/host-pricing',
            views: {
                "viewMain": { templateUrl: 'components/host-pricing/host-pricing.html' },
            }
        })
    $stateProvider
        .state('payment', {
            url: '/payment',
            views: {
                "viewMain": { templateUrl: 'components/payment/payment.html' },
            }
        })
    $stateProvider
        .state('detailRoom', {
            url: '/detailRoom',
            views: {
                "viewMain": { templateUrl: 'components/detailRoom/detailRoom.html' },
            }
        })


}